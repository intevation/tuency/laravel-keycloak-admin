<?php
namespace Scito\Laravel\Keycloak\Admin\Builders;

use Scito\Keycloak\Admin\Representations\UserRepresentationInterface;

/**
 * Class UserRepresentationBuilderAdapter
 *
 * @method static UserRepresentationBuilderAdapter username(?string $username)
 * @method static UserRepresentationBuilderAdapter password(?string $password)
 * @method static UserRepresentationBuilderAdapter enabled(?bool $enabled)
 * @method static UserRepresentationBuilderAdapter email(?string $email)
 * @method static UserRepresentationInterface build()
 *
 * @package Keycloak\Admin\Laravel\Builders
 */
class UserRepresentationBuilderAdapter extends AbstractBuilderAdapter
{

}
